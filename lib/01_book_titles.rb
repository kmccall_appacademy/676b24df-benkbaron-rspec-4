class Book

  attr_accessor :title

  def title
    words = @title.split

    words.map!.with_index do |word, idx|
      if idx == 0 || little_words.include?(word) == false
        word.capitalize
      else
        word
      end
    end

    words.join(" ")
  end

  private
  
  def little_words
    ["in", "the", "a", "and", "of", "an"]
  end

end
