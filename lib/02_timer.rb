class Timer

  def initialize (seconds = 0)
    @seconds = seconds
  end

  attr_accessor :seconds

  def time_string
    secs = seconds % 60
    mins = (seconds / 60) % 60
    hours = seconds / 3600

    if secs < 10
      secs = "0" + secs.to_s
    else
      secs = secs.to_s
    end

    if mins < 10
      mins = "0" + mins.to_s
    else
      mins = mins.to_s
    end

    if hours < 10
      hours = "0" + hours.to_s
    else
      hours = hours.to_s
    end

    "#{hours}:#{mins}:#{secs}"
  end

end
