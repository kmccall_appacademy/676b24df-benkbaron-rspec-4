class Dictionary

  def initialize
    @entries = {}
  end

  attr_reader :entries

  def add(entry)
    if entry.class == Hash
      word = entry.to_a[0][0]
      definition = entry.to_a[0][1]
    else
      word = entry
      definition = nil
    end
    entries[word] = definition
  end

  def keywords
    entries.keys.sort
  end

  def include?(word)
    keywords.include?(word)
  end

  def find(word)
    entries.select { |k, v| k[0...word.length] == word }
  end

  def printable
    list = []
    keywords.each { |word| list << "[#{word}] \"#{entries[word]}\"" }
    list.join("\n")
  end

end
